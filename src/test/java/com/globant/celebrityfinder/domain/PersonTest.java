package com.globant.celebrityfinder.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PersonTest {

	@Test
	public void shouldIdentifyPerson_IgnoringCase() {
		Person person = new Person("Abc");
		assertTrue(person.isCalled("ABC"));
	}
	
	@Test
	public void shouldIndicate_whenThereIsKnownPeople() {
		Person person = new Person("Abc", "first known", "second known");
		assertTrue(person.hasKnownPeople());
	}
	
	@Test
	public void shouldIndicate_whenThereIsNotKnownPeople() {
		Person person = new Person("Abc");
		assertFalse(person.hasKnownPeople());
	}
	
	@Test
	public void shouldFindKnownPerson() {
		Person person = new Person("Abc", "First known", "Second known");
		assertTrue(person.knows("first KNOWN"));
	}
	
	@Test
	public void shouldNotFindUnknownPerson() {
		Person person = new Person("Abc", "First known", "Second known");
		assertFalse(person.knows("Third known"));
	}
	
}
