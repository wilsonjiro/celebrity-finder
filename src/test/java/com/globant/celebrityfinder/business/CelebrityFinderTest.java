package com.globant.celebrityfinder.business;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import com.globant.celebrityfinder.domain.Person;
import com.globant.celebrityfinder.exception.BusinessException;
import com.globant.celebrityfinder.repository.PeopleInMemmoryRepository;

public class CelebrityFinderTest {

	private final Person michaelJackson = new Person("Michael Jackson");
	private CelebrityFinder celebrityFinder;
	
	@Test(expected = BusinessException.EmptyGroupException.class)
	public void whenNoPeople_throwError() {
		celebrityFinder = new CelebrityFinder(new EmptyGroupRepository());
		celebrityFinder.find();
	}

	@Test(expected = BusinessException.CelebrityNotFoundException.class)
	public void whenNoCelebrityInGroup_throwError() {
		celebrityFinder = new CelebrityFinder(new NoCelebrityInGroupRepository());
		celebrityFinder.find();
	}
	
	@Test
	public void whenOnePersonGroupAndIsCelebrity_getCelebrity() {
		celebrityFinder = new CelebrityFinder(new LonelyCelebrityRepository());
		Person celebrity = celebrityFinder.find();
		assertEquals(michaelJackson, celebrity);
	}
	
	@Test
	public void whenOneCelebrityKnownByOthers_getCelebrity() {
		celebrityFinder = new CelebrityFinder(new CompleteGroupRepository());
		Person celebrity = celebrityFinder.find();
		assertEquals(michaelJackson, celebrity);
	}

	/**
	 * I created my own test doubles because they are easy to create and understand.
	 * with this approach, I avoid the inclusion of external libraries for mocking, even though sometimes they are helpful.
	 */
	class EmptyGroupRepository extends PeopleInMemmoryRepository {
		public EmptyGroupRepository() {
			super(null);
		}
	}

	class NoCelebrityInGroupRepository extends PeopleInMemmoryRepository {
		public NoCelebrityInGroupRepository() {
			super(Arrays.asList(
					new Person("Sofia", "Peter"),
					new Person("Charlie", "Maria"))
					);
		}
	}

	class CompleteGroupRepository extends PeopleInMemmoryRepository {
		public CompleteGroupRepository() {
			super(Arrays.asList(
					michaelJackson,
					new Person("Sofia", "Peter", "Michael Jackson"), 
					new Person("Charlie", "Maria", "Michael Jackson"))
					);
		}
	}
	
	class LonelyCelebrityRepository extends PeopleInMemmoryRepository {
		public LonelyCelebrityRepository() {
			super(Arrays.asList(michaelJackson));
		}
	}
	
}
