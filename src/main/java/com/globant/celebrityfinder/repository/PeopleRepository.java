package com.globant.celebrityfinder.repository;

import java.util.Collection;

import com.globant.celebrityfinder.domain.Person;

public interface PeopleRepository {
	
    Collection<Person> findPeopleWithoutKnowns();

	boolean everyoneElseKnowsPerson(String name);

	boolean isEmpty();
    
}
