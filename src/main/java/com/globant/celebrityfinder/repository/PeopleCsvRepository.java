package com.globant.celebrityfinder.repository;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.Scanner;

import com.globant.celebrityfinder.domain.Person;
import com.globant.celebrityfinder.exception.InvalidFormatException;

public class PeopleCsvRepository extends PeopleInMemmoryRepository {

	public PeopleCsvRepository(final String csvPath) throws IOException {
		if (isBlank(csvPath))
			throw new IllegalArgumentException("Path for CSV file cannot be empty");
		
		super.group = loadGroupOfPeople(Paths.get(csvPath));
	}
	
	//If we have different ways to load the group of People, it is recommended to implement a Builder pattern. I didn't do it because of time constraints.
	private Collection<Person> loadGroupOfPeople(final Path csvPath) throws IOException {
		Collection<Person> group = new ArrayList<>();
		try (Scanner scanner = new Scanner(csvPath);) {
			while (scanner.hasNextLine()) {
				final Optional<Person> person = buildPersonFromLine(scanner.nextLine());
				if(person.isPresent())
					group.add(person.get());
			}
		}
		return group;
	}

	private Optional<Person> buildPersonFromLine(final String line) {
		try (Scanner rowScanner = new Scanner(line)) {
			rowScanner.useDelimiter(",");
			
			String name = null;
			Collection<String> knowns = new ArrayList<>();

			if (rowScanner.hasNext())
				name = standardizeNameFormat(rowScanner.next());
			
			while (rowScanner.hasNext())
				knowns.add(standardizeNameFormat(rowScanner.next()));
			
			if (!isBlank(name))
				return Optional.of(new Person(name, knowns));
			
			return Optional.empty();
			
		} catch (final Exception e) {
			throw new InvalidFormatException(String.format("error loading person from line [%s] in csv file", line));
		}
	}
	
	private String standardizeNameFormat(final String name) {
		return name.trim();
	}
	
	private boolean isBlank(String s) {
		return s == null || s.trim().length() == 0;
	}

}