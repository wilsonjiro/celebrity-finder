package com.globant.celebrityfinder.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import com.globant.celebrityfinder.domain.Person;

public class PeopleInMemmoryRepository implements PeopleRepository {

	protected Collection<Person> group = new ArrayList<>();
	
	public PeopleInMemmoryRepository() {
	}
	
	public PeopleInMemmoryRepository(final Collection<Person> people) {
		if (people != null)
			this.group = people;
	}
	
	@Override
	public Collection<Person> findPeopleWithoutKnowns() {
		return group.stream()
				.filter(person -> !person.hasKnownPeople())
				.collect(Collectors.toList());
	}
	
	@Override
	public boolean everyoneElseKnowsPerson(String name) {
		return !group.stream()
				.filter(person -> !person.isCalled(name))
				.anyMatch(person -> !person.knows(name));
	}
	
	@Override
	public boolean isEmpty() {
		return group.isEmpty();
	}
	
}
