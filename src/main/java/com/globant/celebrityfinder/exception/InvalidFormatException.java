package com.globant.celebrityfinder.exception;

public class InvalidFormatException extends RuntimeException {

	private static final long serialVersionUID = -7957980955933851608L;

	public InvalidFormatException(String message) {
		super(message);
	}
	
}
