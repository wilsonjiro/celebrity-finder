package com.globant.celebrityfinder.exception;

/**
 * Grouping of exceptions related to business rules violations.
 * They extend RuntimeException to make the code free of explicitly indicating the type of exceptions being thrown
 * @author wilson.rodriguez
 *
 */
public class BusinessException {

	private BusinessException() {
	}

	public static class CelebrityNotFoundException extends RuntimeException {
		private static final long serialVersionUID = 1018206881494209833L;
		
		public CelebrityNotFoundException() {
			super("Celebrity could not be found");
		}
	}
	
	public static class EmptyGroupException extends RuntimeException {
		private static final long serialVersionUID = 1018206881494209833L;
		
		public EmptyGroupException() {
			super("The group is empty, so there is no celebrity.");
		}
	}
	
}
