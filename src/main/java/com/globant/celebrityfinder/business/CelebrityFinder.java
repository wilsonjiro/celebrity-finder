package com.globant.celebrityfinder.business;

import java.util.Collection;

import com.globant.celebrityfinder.domain.Person;
import com.globant.celebrityfinder.exception.BusinessException;
import com.globant.celebrityfinder.repository.PeopleRepository;

public class CelebrityFinder {

	private final PeopleRepository peopleRepository;

	public CelebrityFinder(final PeopleRepository peopleRepository) {
		this.peopleRepository = peopleRepository;
	}

	public Person find() {
		
		if (peopleRepository.isEmpty())
			throw new BusinessException.EmptyGroupException();
		
		final Collection<Person> peopleWithoutKnowns = peopleRepository.findPeopleWithoutKnowns();

		if (peopleWithoutKnowns == null || peopleWithoutKnowns.isEmpty())
			throw new BusinessException.CelebrityNotFoundException();
		
		Person likelyCelebrity = peopleWithoutKnowns.iterator().next();

		final boolean everyoneKnowsPerson = peopleRepository.everyoneElseKnowsPerson(likelyCelebrity.getName());

		if (everyoneKnowsPerson)
			return likelyCelebrity;
		else
			throw new BusinessException.CelebrityNotFoundException();

	}

}
