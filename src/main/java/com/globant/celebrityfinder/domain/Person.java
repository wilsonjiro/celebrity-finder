package com.globant.celebrityfinder.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public class Person {
	
	private final String name;
	private final Collection<String> knownPeople;
	
	public Person(final String name, final Collection<String> knownPeople) {
		Objects.requireNonNull(name, "Person name must not be null");
		this.name = name;
		if (knownPeople != null)
			this.knownPeople = knownPeople;
		else
			this.knownPeople = Collections.emptyList();
	}

	public Person(final String name, final String... knownPeople) {
		Objects.requireNonNull(name, "Person name must not be null");
		this.name = name;
		if (knownPeople != null)
			this.knownPeople = Arrays.asList(knownPeople);
		else
			this.knownPeople = Collections.emptyList();
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isCalled(final String name) {
		return this.name.equalsIgnoreCase(name);
	}
	
	public boolean hasKnownPeople() {
		return knownPeople != null && !knownPeople.isEmpty();
	}
	
	public boolean knows(final String name) {
		return knownPeople.stream().anyMatch(pName -> pName.equalsIgnoreCase(name));
	}

}
