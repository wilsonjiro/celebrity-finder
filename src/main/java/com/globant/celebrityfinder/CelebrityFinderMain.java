package com.globant.celebrityfinder;

import com.globant.celebrityfinder.business.CelebrityFinder;
import com.globant.celebrityfinder.domain.Person;
import com.globant.celebrityfinder.repository.PeopleCsvRepository;
import com.globant.celebrityfinder.repository.PeopleRepository;

/**
 * This application reads a team or group of people where there is just one
 * celebrity. The data is loaded from a CSV file located in folder
 * "resources/people_group.csv".<br>
 * 
 * The structure of each record in the file is:<br>
 * Person Id, Person Name, Id of the known celebrity<br>
 * The Id of the known celebrity can be empty, because a celebrity is supposed
 * to know nobody.<br>
 * No commented lines are allowed.
 * 
 * @author wilson.rodriguez
 *
 */
public class CelebrityFinderMain {

	public static void main(String[] args) {
		try {
			executeCelebrityFindingProcess("resources\\people_group.csv");
		} catch (Exception e) {
			System.err.println(
					String.format("Error trying to find the celebrity in the team. ErrorType: %s. details: %s",
					e.getClass().getName(), e.getMessage()));
		}
	}

	private static void executeCelebrityFindingProcess(String csvPath) throws Exception {
		PeopleRepository peopleRepository = new PeopleCsvRepository(csvPath);
		CelebrityFinder celebrityFinder = new CelebrityFinder(peopleRepository);
		Person celebrity = celebrityFinder.find();
		System.out.println(String.format("Congratulations! Found celebrity [%s] in the team", celebrity.getName()));
	}

}
