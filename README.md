#CELEBRITY FINDER

This application reads a team or group of people where there is allowed just one celebrity.
The data is loaded from a CSV file located in folder "resources/people_group.csv".

The structure of each record in the file is:

	Person Name, First known person, Second known person[,other knowns...]
	
The only required column is Person Name.
The only person that knows nobody is the celebrity. Everyone else knows the celebrity.
You can add person as a known person even if they are not in the group.
Empty lines are allowed in the file.

A valid example for the content of the file is:

Michael Jackson
Jhon, Michael Jackson
Thomas, Charlie, Michael Jackson, Jhon
Maria, Michael Jackson, Jhon

Where, according to the rules, "Michael Jackson" is the celebrity, and all the other people in the group have a reference to that celebrity.

##PREREQUSITES
* JDK 1.8 or higher installed and available on your PATH
* Gladle 5.5+ installed and available on your PATH

##HOW TO RUN
* Clone the repository:
```
git clone https://wilsonjiro@bitbucket.org/wilsonjiro/celebrity-finder.git
```
* Modify the file "resources/people_group.csv" according to the group of people you want to configure. You can also use the sample data.
* Build the project using the command prompt in the directory of the application (celebrity-finder):
```
$ gradle clean build
```
If all the tests passed, you will see a "BUILD SUCCESSFUL message".
* Run the application
```
$ java -jar build/libs/celebrity-finder.jar
```
* If the application finds the celebrity, you will see in the console the text:
```
	Congratulations! Found celebrity [Michael Jackson] in the team
```
Otherwise, you will see the error generated.